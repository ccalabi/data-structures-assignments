//This program was created by Cori Calabi, ccalabi.  It plays the game Left, Right, Center based 
//on user input of the number of players and a seed for rolling the dice randomly (pseudo).

#include <stdio.h> //For printf and scanf
#include <stdlib.h> //For rand and srand

int main() 
{
  
  //Player names
  const char *names[] = {"Whoopi",
                         "Dale",
                         "Rosie",
                         "Jimmie",
                         "Barbara",
                         "Kyle",
                         "Raven",
                         "Tony",
                         "Jenny",
                         "Clint"};
  //I didn't find it necessary to use these two lines, sorry if they should have been used
  //typedef enum faciem {LEFT, RIGHT, CENTER, PASS} faces;
  //faces die[] = {LEFT, RIGHT, CENTER, PASS, PASS, PASS};

  int numPlayers; //stores the user-entered number of players
  unsigned int seed;
  int randomNumber; 
  int numRolls; //stores how many rolls each player gets, based on how much money they have
  int money[] = {3,3,3,3,3,3,3,3,3,3}; //how much money each player has, corresponds to the names array
  int centerPot = 0; //how much money is in the center pot
  int playersIn; //how many players have money and so can roll the dice
  
  //Ask the user to enter the number of players and store it
  printf("Please enter number of players: \n");
  scanf("%d", &numPlayers);
 
  //Ask the user to enter the seed
  printf("Please enter a seed: \n");
  scanf("%u", &seed);
  srand(seed);  //sets seed to what was entered by user
  playersIn = numPlayers;


  //while 2 or more players have money, the game continues
  while (playersIn > 1) 
  {
    for (int j=0; j<numPlayers; j++) 
    {
      //if the next player in line has money, they roll and play
      if (money[j]>0) 
      {
        printf("%s", names[j]);
        printf(" rolls..."); 

        //roll the dice, rolling 1 die for each dollar they have, up to three dice 
        numRolls = money[j];
        if (numRolls > 3) 
        {
          numRolls = 3;
        }
        for (int i=0; i<numRolls; i++) 
        {
          randomNumber = rand() % 6;  //The %6 limits the random number to between 0 and 5

          //If the random number is 0: an "L" is on the die: LEFT
          if (randomNumber == 0) 
          {
            printf("gives $1 to ");
            money[j]--;  //take $1 from player who rolled
            //give $1 to player on left, need if else in order to go "around" the array to the other end
            if (j-1 >= 0) 
            {
              money[j-1]++;
              printf("%s", names[j-1]);
            }
            else 
            {
              money[numPlayers-1]++;
              printf("%s", names[numPlayers-1]);
            }
          }

          //If the random number is 1: an "R" is on the die: RIGHT
          else if (randomNumber == 1) 
          {
            printf("gives $1 to ");
            money[j]--;
            //give $1 to player on the right, if else needed as above
            if (j+1 < numPlayers) 
            {
              money[j+1]++;
              printf("%s", names[j+1]);
            }
            else 
            {
              money[0]++;
              printf("%s", names[0]);
            }
          }
         
          //If the random number is 2: a "C" is on the die: CENTER
          else if (randomNumber == 2) 
          {  //puts $1 in center pot
            printf("puts $1 in center pot");
            money[j]--;
            centerPot++;
          }
       
          //If the random number is a 3, 4, or 5: a "dot" is on the die: PASS
          else 
          {
            printf("gets a pass");
          }
          printf(", "); 
        }
        
        playersIn = numPlayers;
        //check how many players have money, and end the game if only one has money
        for (int n=0; n<numPlayers; n++) 
        {
          if (money[n] == 0) 
          {
            playersIn--;
          }
        }
        printf("\n");
        //if only one person has money left, the game ends and that person wins
        if (playersIn == 1)
        {
          break;
        }
      }
    }
  }

  //when a player wins, print the amount in their hand and in the pot
  if (playersIn == 1) 
  {
    for (int k=0; k<numPlayers; k++) 
    {
      if (money[k] > 0) 
      {
        printf("%s wins the $%d in the center pot with $%d left in their hand.\n", names[k], centerPot, money[k]);
      }
    }
  }
 
  
  return 0;
}


